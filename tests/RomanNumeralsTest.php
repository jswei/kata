<?php

use PHPUnit\Framework\TestCase;

use App\RomanNumerals;

/**
 * RomanNumeralsTest
 * @group roman
 */
class RomanNumeralsTest extends TestCase
{
    /** 
     * @test 
     * @dataProvider check
     * */
    public function it_generate_a_roman_numeral($number, $expect)
    {
        $numeral = new RomanNumerals;

        $this->assertEquals($expect, $numeral->generate($number));
    }

    /**
     * @test
     */
    public function it_generate_a_roman_numeral_not_over_3999()
    {
        $numeral = new RomanNumerals;

        $this->assertEquals(false, $numeral->generate(4000));
    }

    public function check()
    {
        return [
            ['1', 'I'],
            ['2', 'II'],
            ['3', 'III'],
            ['4', 'IV'],
            ['5', 'V'],
            ['10', 'X'],
            ['14', 'XIV'],
            ['17', 'XVII'],
            ['19', 'XIX'],
            ['50', 'L'],
            ['100', 'C'],
            ['500', 'D'],
            ['900', 'CM'],
            ['1000', 'M'],
            ['3999', 'MMMCMXCIX'],
        ];
    }
}
