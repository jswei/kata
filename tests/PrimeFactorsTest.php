<?php

use PHPUnit\Framework\TestCase;
use App\PrimeFactors;

/**
 * PrimeFactorsTest
 * @group prime
 */
class PrimeFactorsTest extends TestCase
{
    /** 
     * @test 
     * @dataProvider factors
     * */
    public function it_generates_prime_factor_for_1($number, $expect)
    {
        $factors = new PrimeFactors();

        $this->assertEquals($expect, $factors->generate($number));
    }

    public function factors()
    {
        return [
            [1, []],
            [2, [2]],
            [3, [3]],
            [4, [2, 2]],
            [5, [5]],
            [6, [2, 3]],
            [36, [2, 2, 3, 3]],
        ];
    }
}
