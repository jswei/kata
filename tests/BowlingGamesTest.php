<?php

use PHPUnit\Framework\TestCase;

use App\Game;

/**
 * BowlingGamesTest
 * @group bowling
 */
class BowlingGamesTest extends TestCase
{
    /** @test */
    public function it_score_all_zero()
    {
        $game = new Game();

        for ($i = 0; $i < 20; $i++) {
            $game->roll(0);
        }

        $this->assertSame(0, $game->score());
    }

    /** @test */
    public function it_score_all_1()
    {
        $game = new Game();

        for ($i = 0; $i < 20; $i++) {
            $game->roll(1);
        }

        $this->assertSame(20, $game->score());
    }

    /** @test */
    public function it_awards_a_one_roll_bonus_for_every_spare()
    {
        $game = new Game();

        $game->roll(5);
        $game->roll(5); // spare

        $game->roll(8);

        for ($i = 0; $i < 17; $i++) {
            $game->roll(0);
        }

        $this->assertSame(26, $game->score());
    }

    /** @test */
    public function it_awards_a_two_roll_bonus_for_every_strike()
    {
        $game = new Game();

        $game->roll(10); // strike

        $game->roll(5);
        $game->roll(2);

        for ($i = 0; $i < 16; $i++) {
            $game->roll(0);
        }

        $this->assertSame(24, $game->score());
    }

    /** @test */
    public function a_spare_on_the_final_frame_grants_two_extra_balls()
    {
        $game = new Game();

        for ($i = 0; $i < 18; $i++) {
            $game->roll(0);
        }

        $game->roll(5);
        $game->roll(5); // spare

        $game->roll(5); // bonus

        $this->assertSame(15, $game->score());
    }

    /** @test */
    public function a_strike_on_the_final_frame_grants_two_extra_balls()
    {
        $game = new Game();

        for ($i = 0; $i < 18; $i++) {
            $game->roll(0);
        }

        $game->roll(10); // strike

        $game->roll(10);
        $game->roll(10);

        $this->assertSame(30, $game->score());
    }

    /** @test */
    public function it_scores_a_perfact_game()
    {
        $game = new Game();

        for ($i = 0; $i < 12; $i++) {
            $game->roll(10);
        }

        $this->assertSame(300, $game->score());
    }
}
