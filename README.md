# Kata Practice

## Prime Factors

1 = []   
2 = [2]  
3 = [3]  
4 = [2,2]  
6 = [2,3]  
100 = [2,2,5,5]  

## Roman Numerals
1 = I  
2 = II  
3 = III  
4 = IV  
5 = V  
10 = X  
14 = XIV  
17 = XVII  
19 = XIX  
50 = L  
100 = C  
500 = D  
900 = CM  
1000 = M  