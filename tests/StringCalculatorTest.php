<?php

use PHPUnit\Framework\TestCase;
use App\StringCalculator;

/**
 * StringCalculatorTest
 * @group group
 */
class StringCalculatorTest extends TestCase
{
    /** @test */
    public function it_evaluates_an_empty_string_as_zero()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(0, $calculator->add(""));
    }

    /** @test */
    public function it_finds_the_sum_of_a_single_number()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(5, $calculator->add("5"));
    }

    /** @test */
    public function it_finds_the_sum_of_two_number()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(13, $calculator->add("5,8"));
    }

    /** @test */
    public function it_finds_the_sum_of_any_amount_of_number()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(27, $calculator->add("5,8,7,7"));
    }

    /** @test */
    public function it_accepts_a_new_line_character_as_delimiter_too()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(13, $calculator->add("5\n8"));
    }

    /** @test */
    public function it_supports_custom_delimiters()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(20, $calculator->add("//:\n5:8:7"));
    }

    /** @test */
    public function negtive_numbers_are_not_allow()
    {
        $calculator = new StringCalculator;

        $this->expectException(\Exception::class);

        $calculator->add("5,-8");
    }

    /** @test */
    public function numbers_are_greater_than_1000_are_ignored()
    {
        $calculator = new StringCalculator;

        $this->assertEquals(5, $calculator->add("5,1001"));
    }
}
