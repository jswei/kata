<?php

namespace app;

class PrimeFactors
{
    public function generate($number)
    {
        $factors = [];
        $divider = 2;

        while ($number > 1) {
            while ($number % $divider == 0) {
                $factors[] = $divider;

                $number = $number / $divider;
            }
            $divider++;
        }

        return $factors;
    }
}
