<?php

namespace App;

class Game
{
    const FRAME_PER_GAME = 10;

    protected $rolls = [];

    public function roll($pin)
    {
        $this->rolls[] = $pin;
    }

    public function score()
    {
        $score = 0;
        $roll = 0;

        for ($i = 0; $i < self::FRAME_PER_GAME; $i++) {
            // check strike
            if ($this->isStrike($roll)) {
                $score += $this->pinCount($roll);
                $score += $this->strikeBonus($roll);

                $roll += 1;

                continue;
            }

            $score += $this->defaultFrameScore($roll);

            // check spare
            if ($this->isSpare($roll)) {
                $score += $this->spareBonus($roll);
            }
            $roll += 2;
        }

        return $score;
    }

    protected function isStrike(int $roll): bool
    {
        return $this->pinCount($roll) === 10;
    }

    protected function isSpare(int $roll): bool
    {
        return $this->pinCount($roll) +  $this->rolls[$roll + 1] === 10;
    }

    protected function strikeBonus(int $roll): int
    {
        return $this->rolls[$roll + 1] + $this->rolls[$roll + 2];
    }

    protected function spareBonus(int $roll): int
    {
        return $this->rolls[$roll + 2];
    }

    protected function defaultFrameScore(int $roll): int
    {
        return $this->pinCount($roll) + $this->rolls[$roll + 1];
    }

    protected function pinCount(int $roll): int
    {
        return $this->rolls[$roll];
    }
}
