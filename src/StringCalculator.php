<?php

namespace App;

use Exception;

class StringCalculator
{
    const MAX_NUMBER_ALLOW = 1000;

    protected $delimiter = ",|\n";

    public function add(string $number)
    {
        $this->disallowNegtiveNumber($number = $this->parseString($number));
        $this->ignoreGreaterThanThousands($number);

        return array_sum(
            $this->ignoreGreaterThanThousands($number)
        );
    }

    protected function disallowNegtiveNumber($number)
    {
        foreach ($number as $value) {
            if ($value < 0) {
                throw new Exception("Negative numbers are disallow.");
            }
        }
    }

    protected function parseString($number)
    {
        $customDelimiter = "\/\/(.)\n";

        if (preg_match("/{$customDelimiter}/", $number, $matches)) {
            $this->delimiter = $matches[1];

            $number = \str_replace($matches[0], '', $number);
        }

        return preg_split("/{$this->delimiter}/", $number);
    }

    protected function ignoreGreaterThanThousands($number)
    {
        return array_filter($number, function ($number) {
            return $number <= self::MAX_NUMBER_ALLOW;
        });
    }
}
